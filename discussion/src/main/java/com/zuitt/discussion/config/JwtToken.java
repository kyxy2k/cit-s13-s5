package com.zuitt.discussion.config;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = 6616306621458523551L;

    //time duration in secs that the token can be used;

    public static final long JWT_TOKEN_VALIDITY = 5*60*60;

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
        // .setClaims includes the information to show the recipient which is the username
        // .setSubject adds information about the subject. (The subject username.)
        // .setIssuedAt sets the time and date when the token was created
        // .setExpiration sets the expiration of the token
        // .signWith creates the token using a declared algorithm, with the secret keyword.
        // "HS512" is a secure cryptographic algorithm.
        // The "secret key" is passed as a parameter and is used to verify the signature later on.
        // .compact() is used to generate the final JWT string by compacting the JWT builder object.
        return Jwts.builder().setClaims(claims).setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()+ JWT_TOKEN_VALIDITY*1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }
    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims =new HashMap<>();

        User user = userRepository.findByUsername(userDetails.getUsername());

        claims.put("user", user.getId());

        return doGenerateToken(claims, userDetails.getUsername());
    }
    public Boolean validateToken(String token, UserDetails userDetails){
        final String username = getUsernameFromToken(token);

        return (username.equals(userDetails.getUsername())&& !isTokenExpired(token));
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T>claimsResolver){
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken (String token){
        return  Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    public String getUsernameFromToken(String token){
        String claim = getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

    public Date getExpirationDateFromToken(String token){
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private Boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromToken(token);

        return expiration.before(new Date());
    }
}
