package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserService {
    Optional<User> findByUsername(String username);

    ResponseEntity createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User post);
}
