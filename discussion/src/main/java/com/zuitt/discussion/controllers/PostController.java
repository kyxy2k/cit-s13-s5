package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;


    // Create Post
    //@PostMapping("/posts")
    @RequestMapping(value = "/posts",method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken,@RequestBody Post post){
        postService.createPost(stringToken,post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    @GetMapping("/posts")
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid,@RequestHeader(value="Authorization") String stringToken){
        return postService.deletePost(stringToken,postid);
    }

    //    Update a post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedPost(@PathVariable Long postid,@RequestHeader(value="Authorization")String stringToken, @RequestBody Post post){
        return postService.updatePost(postid,stringToken, post);
    }
    //  Get user post
    @GetMapping("/myPost")
    public ResponseEntity<Object> getMyPost(@RequestHeader(value="Authorization")String stringToken){
        return new ResponseEntity<>(postService.getMyPosts(stringToken),HttpStatus.OK);

    }



}
